# xkb-keyboard-layout-util

This repo contains two scripts:
 - `gen_map.sh` generates a keymap based on options specified within the script, compiles it and opens a document viewer showing a visualization of the map
 - `gen_map_more_ids.sh` only generates a keymap, but is more compatible with the extra keycodes the wayland ecosystem understands (>255)

 The generated .xkb files can then be loaded with the desired application.

#!/usr/bin/env bash

set -euo pipefail

# Generate the map according to set params
setxkbmap -print -layout de,us -option "grp:shifts_toggle" -option "caps:escape_shifted_capslock" -option "altwin:swap_lalt_lwin" -option "compose:rwin" -option "shift:breaks_caps" > map.xkbdef

# Generate a .xkb file that can be loaded to apply the map
xkbcomp -xkb -o map.xkb map.xkbdef

# Generate a .xkm file that is used for the visualization
xkbcomp -xkm -o map.xkm map.xkb

# Generate the visualization
xkbprint -color -label symbols -o map.ps map.xkm

# Open the visualization with the default viewer
xdg-open map.ps

# Cleanup
rm map.xkm map.xkbdef

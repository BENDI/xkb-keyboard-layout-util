#!/usr/bin/env bash

set -euo pipefail

# generate the map
xkbcli compile-keymap --layout us,de --model applealu_iso --options "grp:shifts_toggle,caps:escape_shifted_capslock,altwin:swap_lalt_lwin,compose:rwin" > map_more_ids.xkb

# Edit the file in place
sed -i -e "s/	<LCTL>               = 37;/	<LCTL>               = 472;/" -e "s/	<I472>               = 472;/	<I472>               = 37;/" map_more_ids.xkb
